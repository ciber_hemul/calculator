//
// Created by Andrey Gostev on 29.01.2022.
//
#pragma once
#include "Calculator.h"

Calculator::Calculator(string Expr): m_Expr(std::move(Expr)), m_result(), m_failed(false)  {}

Calculator::Calculator(const char *Expr): m_Expr(Expr), m_result(), m_failed(false) {}

Calculator::~Calculator() = default;

bool Calculator::calculate() {
    this->m_Expr = regex_replace(this->m_Expr, regex(R"(\s)"), "");

    if (count(this->m_Expr.begin(), this->m_Expr.end(), '(') != count(this->m_Expr.begin(), this->m_Expr.end(), ')')) {
        this->m_failed = true;
        return false;
    }

    smatch Matches;

    if (regex_search(this->m_Expr, Matches, regex(R"([^(\+|\-|\*|\/|sqrt(?=\(\d+\.?\d*\))|\(|\)|\d|\.)])"))) {
        this->m_failed = true;
        return false;
    }

    m_result = calcExpr(this->m_Expr);

    return true;
}

inline double Calculator::calcExpr(string Expr) {
    static smatch Matches;
    try {
        if (regex_match(Expr, Matches, regex(R"((?:\()[^\(\)]*(?:\)))"))) {
            Expr = Expr.substr(1, Expr.size() - 2);
        }

        while (regex_search(Expr, Matches, regex(R"(sqrt(?:\()[^\(\)]*(?:\)))"))) {
            double res = calcExpr(Matches.str().substr(4));
            if (res < 0) {
                this->m_failed = true;
                return false;
            }

            Expr = regex_replace(Expr, regex(R"(sqrt(?:\()[^\(\)]*(?:\)))"), to_string(sqrt(res)), regex_constants::format_first_only);
        }

        while (Expr.find('(') != string::npos) {
            regex_search(Expr, Matches, regex(R"((?:\()[^\(\)]*(?:\)))"));
            double res = calcExpr(Matches.str());
            Expr = regex_replace(Expr, regex(R"((?:\()[^\(\)]*(?:\)))"), to_string(res), regex_constants::format_first_only);
        }

        while (Expr.find('/') != string::npos) {
            regex_search(Expr, Matches, regex(R"(\d+(\.\d+)?\/\-?\d+(\.\d+)?)"));
            vector<string> v;
            char* end;
            explode(Matches.str(), v, R"(/)");
            double res = strtod(v[0].c_str(), &end) / strtod(v[1].c_str(), &end);
            Expr = regex_replace(Expr, regex(R"(\d+(\.\d+)?\/\-?\d+(\.\d+)?)"), to_string(res), regex_constants::format_first_only);
        }

        while (Expr.find('*') != string::npos) {
            regex_search(Expr, Matches, regex(R"(\d+(\.\d+)?\*\-?\d+(\.\d+)?)"));
            vector<string> v;
            char* end;
            explode(Matches.str(), v, R"(*)");
            double res = strtod(v[0].c_str(), &end) * strtod(v[1].c_str(), &end);
            Expr = regex_replace(Expr, regex(R"(\d+(\.\d+)?\*\-?\d+(\.\d+)?)"), to_string(res), regex_constants::format_first_only);
        }

        Expr = regex_replace(Expr, regex(R"(\-)"), "+-");
    }
    catch (...) {
        this->m_failed = true;
        return 0;
    }


    char* end;
    vector<string> v;
    double result = 0;
    if (Expr.find('+') != string::npos) {
        explode(Expr, v, "+");

        for (auto& s: v) {
            if (s.empty()) {
                continue;
            }
            result += strtod(s.c_str(), &end);
        }
    } else {
        result = strtod(Expr.c_str(), &end);
    }

    return result;
}

ostream& operator<< (ostream& out, const Calculator& calc) {
    return out << calc.m_result;
}

bool Calculator::isFailed() const {
    return this->m_failed;
}

double Calculator::getResult() const {
    return this->m_result;
}
