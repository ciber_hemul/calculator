//
// Created by Andrey Gostev on 29.01.2022.
//

#include <string>
#include <vector>
#include <iostream>
#include <regex>
#include <utility>
#include <cmath>
#include <cstdlib>
#include <chrono>
#include "conio.h"
#include <thread>
#include <atomic>

using namespace std;

bool explode(string Str, vector<string>& Result, const char* Delim);

bool regex_search_all (string Str, vector<string>& Result, const regex& Re, regex_constants::match_flag_type Flags = regex_constants::match_default);
