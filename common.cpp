//
// Created by Andrey Gostev on 29.01.2022.
//

#include "common.h"

bool explode(string Str, vector<string> &Result, const char *Delim) {
    while (Str.find(Delim) != string::npos) {
        int pos = (int)Str.find(Delim);

        Result.emplace_back(Str.substr(0, pos));
        Str = Str.substr(pos+1);
    }

    Result.emplace_back(Str);

    return true;
}

bool regex_search_all (string Str, vector<string>& Result, const regex& Re, regex_constants::match_flag_type Flags) {
    smatch Matches;

    while (regex_search(Str, Matches, Re)) {
        if (!Matches.suffix().str().empty()) {
            Str = Matches.suffix().str();

            Result.emplace_back(Matches.str());
        }
    }

    return !Result.empty();
}
