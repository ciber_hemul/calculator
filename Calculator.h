//
// Created by Andrey Gostev on 29.01.2022.
//
#pragma  once
#include "common.h"

class Calculator {
private:
    double m_result;
    string m_Expr;
    bool m_failed;

    inline double calcExpr(string Expr);
public:
    explicit Calculator(string Expr);

    explicit Calculator(const char* Expr);

    ~Calculator();

    bool calculate();

    friend ostream& operator<< (ostream& out, const Calculator& calc);

    [[nodiscard]] bool isFailed () const;

    [[nodiscard]] double getResult () const;
};
