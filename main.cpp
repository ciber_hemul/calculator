#pragma once
#include "Calculator.h"

atomic<bool> g_check{false};

int main() {
    string Expr;
    getline(cin, Expr);
    Calculator Calc(Expr);
    Calc.calculate();

    if (Calc.isFailed()) {
        cout << "failed to calculate, check your expression and try again\n";
    }

    thread t{[]() {
        char c;
        while (cin >> c) {
            g_check = true;
            break;
        }
    }};

    for(;;) {
        if (g_check || Calc.isFailed()) {
            break;
        }

        system("cls");
        cout << Calc << "\n";
        this_thread::sleep_for(1000ms);
        cout << "...";
        this_thread::sleep_for(1000ms);
    }

    t.join();

    return EXIT_SUCCESS;
}
